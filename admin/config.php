<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/dragonpunchintl/admin/');
define('HTTP_CATALOG', 'http://localhost/dragonpunchintl/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/dragonpunchintl/admin/');
define('HTTPS_CATALOG', 'http://localhost/dragonpunchintl/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/dragonpunchintl/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/dragonpunchintl/system/');
define('DIR_IMAGE', 'C:/xampp/htdocs/dragonpunchintl/image/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/dragonpunchintl/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/dragonpunchintl/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/dragonpunchintl/system/config/');
define('DIR_CACHE', 'C:/xampp/htdocs/dragonpunchintl/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/dragonpunchintl/system/storage/download/');
define('DIR_LOGS', 'C:/xampp/htdocs/dragonpunchintl/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/dragonpunchintl/system/storage/modification/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/dragonpunchintl/system/storage/upload/');
define('DIR_CATALOG', 'C:/xampp/htdocs/dragonpunchintl/catalog/');

// DB dump cloudshop_10_11_2017
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'cloudshop');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
